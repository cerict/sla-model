package eu.musaproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "component_control_metrics")
public class ComponentControlMetric {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "component_control_id")
	private ComponentControl componentControl;
	
	@OneToOne
	@JoinColumn(name = "metricId")
	private Metric metric;
	
	@Column(name = "selected")
	private Boolean selected;
	
	@Column(name = "assessedSelected")
	private Boolean assessedSelected;

	@Column(name = "assessedTrue")
	private Boolean assessedTrue;

	@Column(name = "value")
	@Type(type="text")
	private String value;
	
	@Transient
	private List<Control> associatedControls;
	

	public ComponentControlMetric(){}
	
	public ComponentControlMetric(Metric metric, ComponentControl componentControl){
		setComponentControl(componentControl);
		setMetric(metric);
	}

	public ComponentControl getComponentControl() {
		return componentControl;
	}

	public void setComponentControl(ComponentControl componentControl) {
		this.componentControl = componentControl;
	}

	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}	

	public Long getId() {
		return id;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public Boolean getAssessedTrue() {
		return assessedTrue;
	}

	public void setAssessedTrue(Boolean assessedTrue) {
		this.assessedTrue = assessedTrue;
	}
	
	public Boolean getAssessedSelected() {
		return assessedSelected;
	}

	public void setAssessedSelected(Boolean assessedSelected) {
		this.assessedSelected = assessedSelected;
	}

	public List<Control> getAssociatedControls() {
		return associatedControls;
	}

	public void setAssociatedControls(List<Control> associatedControls) {
		this.associatedControls = associatedControls;
	}
}
