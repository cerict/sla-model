package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "component_threats")
public class ComponentThreat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "component_id")
	private Component component;
	
	@OneToOne
	@JoinColumn(name = "threat_id")
	private Threat threat;
	
	@Column(name = "skill_level")
	@Min(0) @Max(9)
	private Integer skillLevel;
	
	@Column(name = "motive")
	@Min(0) @Max(9)
	private Integer motive;
	
	@Column(name = "opportunity")
	@Min(0) @Max(9)
	private Integer opportunity;
	
	@Column(name = "size")
	@Min(0) @Max(9)
	private Integer size;
	
	@Column(name = "ease_of_discovery")
	@Min(0) @Max(9)
	private Integer easeOfDiscovery;
	
	@Column(name = "ease_of_exploit")
	@Min(0) @Max(9)
	private Integer easeOfExploit;
	
	@Column(name = "awareness")
	@Min(0) @Max(9)
	private Integer awareness;
	
	@Column(name = "intrusion_detection")
	@Min(0) @Max(9)
	private Integer intrusionDetection;
	
	@Column(name = "loss_of_confidentiality")
	@Min(0) @Max(9)
	private Integer lossOfConfidentiality;
	
	@Column(name = "loss_of_integrity")
	@Min(0) @Max(9)
	private Integer lossOfIntegrity;
	
	@Column(name = "loss_of_availability")
	@Min(0) @Max(9)
	private Integer lossOfAvailability;
	
	@Column(name = "loss_of_accountability")
	@Min(0) @Max(9)
	private Integer lossOfAccountability;

	@Column(name = "financial_damage")
	@Min(0) @Max(9)
	private Integer financialDamage;

	@Column(name = "reputation_damage")
	@Min(0) @Max(9)
	private Integer reputationDamage;
	
	@Column(name = "non_compliance")
	@Min(0) @Max(9)
	private Integer nonCompliance;
	
	@Column(name = "privacy_violation")
	@Min(0) @Max(9)
	private Integer privacyViolation;
	
	@Column(name = "risk_severity")
	private String riskSeverity;

	@Column(name = "likehood")
	@Min(0) @Max(9)
	private Double likehood;
	
	@Column(name = "impact")
	@Min(0) @Max(9)
	private Double impact;
	
	public ComponentThreat(){
	}
	
	public ComponentThreat(Threat threat){
		setThreat(threat);
		setSkillLevel(threat.getSkillLevel());
		setMotive(threat.getMotive());
		setOpportunity(threat.getOpportunity());
		setSize(threat.getSize());
		setEaseOfDiscovery(threat.getEaseOfDiscovery());
		setEaseOfExploit(threat.getEaseOfExploit());
		setAwareness(threat.getAwareness());
		setIntrusionDetection(threat.getIntrusionDetection());
		setLossOfConfidentiality(threat.getLossOfConfidentiality());
		setLossOfIntegrity(threat.getLossOfIntegrity());
		setLossOfAvailability(threat.getLossOfAvailability());
		setLossOfAccountability(threat.getLossOfAccountability());
		setFinancialDamage(0);
		setReputationDamage(0);
		setNonCompliance(0);
		setPrivacyViolation(0);
		
		Double impact = 0.0;
		Double totaleImpactBusiness = (double) (getFinancialDamage() + getReputationDamage() + getNonCompliance() 
		+ getPrivacyViolation()) / (double)4;
		Double totaleImpactTechnical = (double) (getLossOfConfidentiality() + getLossOfIntegrity() + getLossOfAvailability() 
		+ getLossOfAccountability()) / (double)4;
		if(totaleImpactBusiness==0){
			impact=totaleImpactTechnical;
		}else{
			impact=totaleImpactBusiness;
		}
		setImpact(impact);
		
		Double likehood = (double) ((getSkillLevel() + getMotive() + getOpportunity() + getSize() + getEaseOfDiscovery() 
		+ getEaseOfExploit() + getAwareness() + getIntrusionDetection())) / (double)8;
		setLikehood(likehood);
		
		String riskSeverity = "";
		if(impact >= 6){
			if (likehood < 3){
				riskSeverity = "MEDIUM";
			}else if (likehood >= 3 && likehood < 6){
				riskSeverity = "HIGH";
			}else if (likehood >= 6){
				riskSeverity = "CRITICAL";
			}
		}else if(impact >= 3 && impact < 6){
			if (likehood < 3){
				riskSeverity = "LOW";
			}else if (likehood >= 3 && likehood < 6){
				riskSeverity = "MEDIUM";
			}else if (likehood >= 6){
				riskSeverity = "HIGH";
			}
		}else if(impact < 3){
			if (likehood < 3){
				riskSeverity = "VERY LOW";
			}else if (likehood >= 3 && likehood < 6){
				riskSeverity = "LOW";
			}else if (likehood >= 6){
				riskSeverity = "MEDIUM";
			}
		}
		
		setRiskSeverity(riskSeverity);
	}
	
	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public Threat getThreat() {
		return threat;
	}

	public void setThreat(Threat threat) {
		this.threat = threat;
	}

	public Long getId() {
		return id;
	}
	
	public Integer getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(Integer skillLevel) {
		this.skillLevel = skillLevel;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(Integer opportunity) {
		this.opportunity = opportunity;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getEaseOfDiscovery() {
		return easeOfDiscovery;
	}

	public void setEaseOfDiscovery(Integer easeOfDiscovery) {
		this.easeOfDiscovery = easeOfDiscovery;
	}

	public Integer getEaseOfExploit() {
		return easeOfExploit;
	}

	public void setEaseOfExploit(Integer easeOfExploit) {
		this.easeOfExploit = easeOfExploit;
	}

	public Integer getAwareness() {
		return awareness;
	}

	public void setAwareness(Integer awareness) {
		this.awareness = awareness;
	}

	public Integer getIntrusionDetection() {
		return intrusionDetection;
	}

	public void setIntrusionDetection(Integer intrusionDetection) {
		this.intrusionDetection = intrusionDetection;
	}

	public Integer getLossOfConfidentiality() {
		return lossOfConfidentiality;
	}

	public void setLossOfConfidentiality(Integer lossOfConfidentiality) {
		this.lossOfConfidentiality = lossOfConfidentiality;
	}

	public Integer getLossOfIntegrity() {
		return lossOfIntegrity;
	}

	public void setLossOfIntegrity(Integer lossOfIntegrity) {
		this.lossOfIntegrity = lossOfIntegrity;
	}

	public Integer getLossOfAvailability() {
		return lossOfAvailability;
	}

	public void setLossOfAvailability(Integer lossOfAvailability) {
		this.lossOfAvailability = lossOfAvailability;
	}

	public Integer getLossOfAccountability() {
		return lossOfAccountability;
	}

	public void setLossOfAccountability(Integer lossOfAccountability) {
		this.lossOfAccountability = lossOfAccountability;
	}
	
	public Integer getFinancialDamage() {
		return financialDamage;
	}

	public void setFinancialDamage(Integer financialDamage) {
		this.financialDamage = financialDamage;
	}

	public Integer getReputationDamage() {
		return reputationDamage;
	}

	public void setReputationDamage(Integer reputationDamage) {
		this.reputationDamage = reputationDamage;
	}

	public Integer getNonCompliance() {
		return nonCompliance;
	}

	public void setNonCompliance(Integer nonCompliance) {
		this.nonCompliance = nonCompliance;
	}

	public Integer getPrivacyViolation() {
		return privacyViolation;
	}

	public void setPrivacyViolation(Integer privacyViolation) {
		this.privacyViolation = privacyViolation;
	}
	
	public String getRiskSeverity() {
		return riskSeverity;
	}

	public void setRiskSeverity(String riskSeverity) {
		this.riskSeverity = riskSeverity;
	}

	public Double getLikehood() {
		return likehood;
	}

	public void setLikehood(Double likehood) {
		this.likehood = likehood;
	}

	public Double getImpact() {
		return impact;
	}

	public void setImpact(Double impact) {
		this.impact = impact;
	}
	
}
