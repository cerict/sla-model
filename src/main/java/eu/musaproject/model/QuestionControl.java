package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "questions_control")
public class QuestionControl {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "source_name")
	private String sourceName;
	
	@Column(name = "source_id", unique = true)
	private String sourceId;

	@Column(name = "description")
	@Type(type="text")
	private String description; 
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<ComponentType> componentTypes;

	public QuestionControl(){
		componentTypes = new HashSet<ComponentType>();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Long getId() {
		return id;
	}
	
	public void addComponentType(ComponentType componentType){
		getComponentTypes().add(componentType);
	}
	
	public Set<ComponentType> getComponentTypes() {
		return componentTypes;
	}

	public void setComponentTypes(Set<ComponentType> componentTypes) {
		this.componentTypes = componentTypes;
	}
	
	public void resetComponentTypes(){
		componentTypes = new HashSet<ComponentType>();
	}
	
}
